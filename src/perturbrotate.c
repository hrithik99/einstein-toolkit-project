#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include "constants.h"

#include "util_Table.h"

void hrithik_func_perturb(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS

  CCTK_INFO("A");

  CCTK_INT i,j,k, i3D;


   /*print statements for probing the grid dimensions and orientation, also for debugging*/
  CCTK_REAL tempx,tempy,tempz;

    CCTK_INFO("upper coords");

    tempx=CCTK_ORIGIN_SPACE(0) + (cctk_lbnd[0] + cctk_lsh[0]-1 )*CCTK_DELTA_SPACE(0);
    tempy=CCTK_ORIGIN_SPACE(1) + (cctk_lbnd[1] + cctk_lsh[1]-1 )*CCTK_DELTA_SPACE(1);
    tempz=CCTK_ORIGIN_SPACE(2) + (cctk_lbnd[2] + cctk_lsh[2]-1 )*CCTK_DELTA_SPACE(2);

    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,tempx);
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,tempy);
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,tempz);

    
    CCTK_INFO("lower coords");
    tempx=CCTK_ORIGIN_SPACE(0) + (cctk_lbnd[0])*CCTK_DELTA_SPACE(0);
    tempy=CCTK_ORIGIN_SPACE(1) + (cctk_lbnd[1])*CCTK_DELTA_SPACE(1);
    tempz=CCTK_ORIGIN_SPACE(2) + (cctk_lbnd[2])*CCTK_DELTA_SPACE(2); 


    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,tempx);
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,tempy);
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,tempz);

    
  
    CCTK_INFO("spacings");
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" ,CCTK_DELTA_SPACE(0));
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" , CCTK_DELTA_SPACE(1));
    CCTK_VInfo(CCTK_THORNSTRING,"%4.2f" , CCTK_DELTA_SPACE(2));


    CCTK_VInfo(CCTK_THORNSTRING,"%d" ,CCTK_DELTA_SPACE(0));
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , CCTK_DELTA_SPACE(1));
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , CCTK_DELTA_SPACE(2));

    
    CCTK_INFO("upper limit");
    CCTK_VInfo(CCTK_THORNSTRING, "%u" ,cctk_ubnd[0]);
    CCTK_VInfo(CCTK_THORNSTRING,"%u" , cctk_ubnd[1]);
    CCTK_VInfo(CCTK_THORNSTRING, "%u" ,cctk_ubnd[2]);

    
    CCTK_INFO("lower limit");
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,cctk_lbnd[0]);
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , cctk_lbnd[1]);
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,cctk_lbnd[2]);

    
    CCTK_INFO("origin space");
    CCTK_VInfo(CCTK_THORNSTRING, "%4.2f" ,CCTK_ORIGIN_SPACE(0));
    CCTK_VInfo(CCTK_THORNSTRING, "%4.2f" ,CCTK_ORIGIN_SPACE(1));
    CCTK_VInfo(CCTK_THORNSTRING, "%4.2f" ,CCTK_ORIGIN_SPACE(2));

    
    CCTK_INFO("origin space (signed)");
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,CCTK_ORIGIN_SPACE(0));
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,CCTK_ORIGIN_SPACE(1));
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,CCTK_ORIGIN_SPACE(2));


    CCTK_INFO("i3d values:");
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , CCTK_GFINDEX3D(cctkGH, 0, 0, 0));
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , CCTK_GFINDEX3D(cctkGH, 1, 0, 0));
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , CCTK_GFINDEX3D(cctkGH, 0, 1, 0));
    CCTK_VInfo(CCTK_THORNSTRING,"%d" , CCTK_GFINDEX3D(cctkGH, 0, 0, 1));

    CCTK_INFO("number of indices:");
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,cctk_lsh[0]);
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,cctk_lsh[1]);
    CCTK_VInfo(CCTK_THORNSTRING, "%d" ,cctk_lsh[2]);

    

  


/*add perturbation(of the form: y*exp(-r^2)) to rho by going over all grid points*/

/*CCTK_REAL midpoint
CCTK_INT center, ucenter, lcenter;

midpoint=0.5 * cctk_lsh[0];
center=floor(midpoint);
ucenter=center+5;
lcenter=center-5;
*/
  for(k=0; k<cctk_lsh[2]; k++)
    { 
      for(j=0; j<cctk_lsh[1]; j++)
      {
        for(i=0; i<cctk_lsh[0]; i++)
        {     
          i3D=CCTK_GFINDEX3D(cctkGH, i, j, k);
          tempx=CCTK_ORIGIN_SPACE(0) + (cctk_lbnd[0] + i )*CCTK_DELTA_SPACE(0);
          tempy=CCTK_ORIGIN_SPACE(1) + (cctk_lbnd[1] + j )*CCTK_DELTA_SPACE(1);
          tempz=CCTK_ORIGIN_SPACE(2) + (cctk_lbnd[2] + k )*CCTK_DELTA_SPACE(2);
          
          if(-5<tempx && tempx<1 && -5<tempy && tempy<1 && -5<tempz && tempz<1)             
          rho[i3D]=rho[i3D]+p_factor*(abs(tempx+tempy*0.5+tempz)/3);     
          
          /* This is the perturbation yexp(-r^2):rho[i3D]=rho[i3D]*(1+exp(-(i*i+j*j+k*k)*50/((cctk_lsh[1]-1)*(cctk_lsh[1]-1)))*j/(cctk_lsh[1]-1));*/

          /*rho[i3D]=rho[i3D](1+exp(-(i*i+j*j+k*k)*50/((cctk_lsh[1]-1)*(cctk_lsh[1]-1)))*j/(cctk_lsh[1]-1));*/
        } 
      }     
    }

  i3D=CCTK_GFINDEX3D(cctkGH, 20, 20, 20);
  
  CCTK_REAL bigrho, bigeps, bigwlorentz, bigpress;
  bigrho=rho[i3D]*1000;
  bigeps=eps[i3D]*1000;
  bigpress=press[i3D]*1000;
  bigwlorentz=w_lorentz[i3D]*1000;

  CCTK_INFO("hydrobase variable values far from the star:");
  CCTK_VInfo(CCTK_THORNSTRING, "bigrho %4.2f" ,bigrho);
  CCTK_VInfo(CCTK_THORNSTRING, "rho %4.2f" ,rho[i3D]);
  CCTK_VInfo(CCTK_THORNSTRING, "bigeps %4.2f" ,bigeps);
  CCTK_VInfo(CCTK_THORNSTRING, "eps %4.2f" ,eps[i3D]);
  CCTK_VInfo(CCTK_THORNSTRING, "bigwlorentz %4.2f" ,bigwlorentz);
  CCTK_VInfo(CCTK_THORNSTRING, "w_lorentz %4.2f" ,w_lorentz[i3D]);
  CCTK_VInfo(CCTK_THORNSTRING, "bigpress %4.2f" ,bigpress);
  CCTK_VInfo(CCTK_THORNSTRING, "press %4.2f" ,press[i3D]);


  CCTK_REAL lowerx, upperx;  
  CCTK_INT ierrx;  
  ierrx = CCTK_CoordRange(cctkGH, &lowerx, &upperx, 1, NULL, "cart3d");

  CCTK_INFO("ranges xcoords");

  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", lowerx); 
  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", upperx); 

  CCTK_REAL lowery, uppery;  
  CCTK_INT ierry;  
  ierry = CCTK_CoordRange(cctkGH, &lowery, &uppery, 2, NULL, "cart3d");

  CCTK_INFO("ranges ycoords");

  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", lowery); 
  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", uppery); 

  CCTK_REAL lowerz, upperz;  
  CCTK_INT ierrz;  
  ierrz = CCTK_CoordRange(cctkGH, &lowerz, &upperz, 3, NULL, "cart3d");

  CCTK_INFO("ranges zcoords");

  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", lowerz); 
  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", upperz); 

/*finding rho-max coordinates:*/

CCTK_REAL rho_max=0;
CCTK_INT i_max;

  for(k=0; k<cctk_lsh[2]; k++)
    { 
      for(j=0; j<cctk_lsh[1]; j++)
      {
        for(i=0; i<cctk_lsh[0]; i++)
        { 
        i3D=CCTK_GFINDEX3D(cctkGH, i, j, k);
        if(rho[i3D]>rho_max)
        {
          rho_max=rho[i3D];
          i_max=i3D;          
        } 
        }
      }     
    }

  CCTK_VInfo(CCTK_THORNSTRING, "rho_max=%4.2f and i_max=%u", rho_max, i_max);
  CCTK_VInfo(CCTK_THORNSTRING,"%u" , CCTK_GFINDEX3D(cctkGH, 12, 12, 12));
  CCTK_INFO("Perturbation Done"); 

}

void hrithik_func_rotate(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_PARAMETERS

  CCTK_INT i,j,k, i3D;
  /*const int N_INTERP_POINTS=cctk_lsh[0]*cctk_lsh[1]*cctk_lsh[2];*/  
     
  CCTK_REAL tempx,tempy,tempz, tempx1,tempy1,tempz1;

 /*Rotation matrix elements 45 degree rotation about x axis anti-clock

  R00=1;
  R01=0;
  R02=0;
  R10=0;
  R11=1/sqrt(2);
  R12=1/sqrt(2);
  R20=0;
  R21=-1/sqrt(2);
  R22=1/sqrt(2);


  R00=1;
  R01=0;
  R02=0;
  R10=0;
  R11=0;
  R12=1;
  R20=0;
  R21=-1;
  R22=0;

/*-144<tempx1 && tempx1<144 &&-144<tempy1 && tempy1<144 && -144<tempz1 && tempz1<144
*/
  CCTK_REAL lowerx, upperx;  
  CCTK_INT ierrx;  
  ierrx = CCTK_CoordRange(cctkGH, &lowerx, &upperx, 1, NULL, "cart3d");

  CCTK_INFO("ranges xcoords");

  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", lowerx); 
  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", upperx); 

  CCTK_REAL lowery, uppery;  
  CCTK_INT ierry;  
  ierry = CCTK_CoordRange(cctkGH, &lowery, &uppery, 2, NULL, "cart3d");

  CCTK_INFO("ranges ycoords");

  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", lowery); 
  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", uppery); 

  CCTK_REAL lowerz, upperz;  
  CCTK_INT ierrz;  
  ierrz = CCTK_CoordRange(cctkGH, &lowerz, &upperz, 3, NULL, "cart3d");

  CCTK_INFO("ranges zcoords");

  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", lowerz); 
  CCTK_VInfo(CCTK_THORNSTRING, "%4.2f", upperz); 



/*The following is a test loop that I made to debug an error I was experiencing previously*/

  CCTK_INT N_POINTS=0;

  for(k=0; k<cctk_lsh[2]; k++)
    { 
      for(j=0; j<cctk_lsh[1]; j++)
      {
        for(i=0; i<cctk_lsh[0]; i++)
        {          
                    
          tempx=CCTK_ORIGIN_SPACE(0) + (cctk_lbnd[0] + i )*CCTK_DELTA_SPACE(0);
          tempy=CCTK_ORIGIN_SPACE(1) + (cctk_lbnd[1] + j )*CCTK_DELTA_SPACE(1);
          tempz=CCTK_ORIGIN_SPACE(2) + (cctk_lbnd[2] + k )*CCTK_DELTA_SPACE(2);
          
          /*the new coordinates where interpolation has to be done are put in the interpx,y,z arrays as follows:*/
          tempx1=(R00*tempx+R01*tempy+R02*tempz);
          tempy1=(R10*tempx+R11*tempy+R12*tempz);
          tempz1=(R20*tempx+R21*tempy+R22*tempz);

          if(lowerx<tempx1 && tempx1<upperx && lowery<tempy1 && tempy1<uppery && lowerz<tempz1 && tempz1<upperz)
          {
          N_POINTS++;
          }
          
        } 
      }     
    }
CCTK_INFO("npointy");

const CCTK_INT N_INTERP_POINTS=N_POINTS;
CCTK_INFO("npointy");
  CCTK_VInfo(CCTK_THORNSTRING,"no. of interp points=%u", N_INTERP_POINTS);

CCTK_REAL * interp_x = malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);
CCTK_REAL * interp_y = malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);
CCTK_REAL * interp_z = malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);

/*CCTK_REAL interp_x[N_INTERP_POINTS],interp_y[N_INTERP_POINTS],interp_z[N_INTERP_POINTS];*/
CCTK_INFO("npointy");
  CCTK_INFO("Test assignment Done");

  CCTK_INT n=-1;

  for(k=0; k<cctk_lsh[2]; k++)
    { 
      for(j=0; j<cctk_lsh[1]; j++)
      {
        for(i=0; i<cctk_lsh[0]; i++)
        {          

          /*The tempx,y,z are assigned the x,y,z coordinate values*/
         
          tempx=CCTK_ORIGIN_SPACE(0) + (cctk_lbnd[0] + i )*CCTK_DELTA_SPACE(0);
          tempy=CCTK_ORIGIN_SPACE(1) + (cctk_lbnd[1] + j )*CCTK_DELTA_SPACE(1);
          tempz=CCTK_ORIGIN_SPACE(2) + (cctk_lbnd[2] + k )*CCTK_DELTA_SPACE(2);
          
          /*the new coordinates where interpolation has to be done are put in the interpx,y,z arrays as follows:*/
          tempx1=(R00*tempx+R01*tempy+R02*tempz);
          tempy1=(R10*tempx+R11*tempy+R12*tempz);
          tempz1=(R20*tempx+R21*tempy+R22*tempz);

          if(lowerx<tempx1 && tempx1<upperx && lowery<tempy1 && tempy1<uppery && lowerz<tempz1 && tempz1<upperz)          {
          n++;
          interp_x[n]=tempx1;
          interp_y[n]=tempy1;
          interp_z[n]=tempz1;
          } 
        } 
      }     
    }

    CCTK_INFO("Array for interpolation points constructed.");
  



  const void *interp_coords[3];

  CCTK_INFO("a");
    interp_coords[0] = (const void *) interp_x; 
  CCTK_INFO("b");    
    interp_coords[1] = (const void *) interp_y;
      CCTK_INFO("c");
    interp_coords[2] = (const void *) interp_z;
      CCTK_INFO("d");

    CCTK_INT input_array_variable_indices[4];
      CCTK_INFO("e");

    input_array_variable_indices[0] = CCTK_VarIndex("HydroBase::rho");
    input_array_variable_indices[1] = CCTK_VarIndex("HydroBase::eps");
    input_array_variable_indices[2] = CCTK_VarIndex("HydroBase::press");
    input_array_variable_indices[3] = CCTK_VarIndex("HydroBase::w_lorentz");

  /*const cGH *GH;*/
    
  static const CCTK_INT output_array_type_codes[4]= { CCTK_VARIABLE_REAL, CCTK_VARIABLE_REAL,
    CCTK_VARIABLE_REAL,CCTK_VARIABLE_REAL};

  void *output_arrays[4];

  CCTK_REAL * output_rho=malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);
  CCTK_REAL * output_eps=malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);
  CCTK_REAL * output_press=malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);
  CCTK_REAL * output_w_lorentz=malloc(sizeof(CCTK_REAL)*N_INTERP_POINTS);

/*
  CCTK_REAL output_eps[N_INTERP_POINTS];
  CCTK_REAL output_press[N_INTERP_POINTS];
  CCTK_REAL output_w_lorentz[N_INTERP_POINTS];
*/

  output_arrays[0] = (void *) output_rho;
  output_arrays[1] = (void *) output_eps;
  output_arrays[2] = (void *) output_press;
  output_arrays[3] = (void *) output_w_lorentz;

    CCTK_INFO("BeforeInterp");

    CCTK_InterpGridArrays(cctkGH,3,CCTK_InterpHandle("generalized polynomial interpolation"),
     Util_TableCreateFromString("order=4"),CCTK_CoordSystemHandle("cart3d"),N_INTERP_POINTS,
     CCTK_VARIABLE_REAL,interp_coords,4,
     input_array_variable_indices,4,
     output_array_type_codes,output_arrays);

    CCTK_INFO("Interp");

    n=-1;

  CCTK_INT far;
  far=CCTK_GFINDEX3D(cctkGH, 20, 20, 20);

  /*Fetch the output of the intergridarrays function*/  

  for(k=0; k<cctk_lsh[2]; k++)
    { 
      for(j=0; j<cctk_lsh[1]; j++)
      {
        for(i=0; i<cctk_lsh[0]; i++)
        {

          tempx=CCTK_ORIGIN_SPACE(0) + (cctk_lbnd[0] + i )*CCTK_DELTA_SPACE(0);
          tempy=CCTK_ORIGIN_SPACE(1) + (cctk_lbnd[1] + j )*CCTK_DELTA_SPACE(1);
          tempz=CCTK_ORIGIN_SPACE(2) + (cctk_lbnd[2] + k )*CCTK_DELTA_SPACE(2);
          
          /*the new coordinates where interpolation has to be done are put in the interp x,y,z arrays as follows:*/
          tempx1=(R00*tempx+R01*tempy+R02*tempz);
          tempy1=(R10*tempx+R11*tempy+R12*tempz);
          tempz1=(R20*tempx+R21*tempy+R22*tempz);
          CCTK_INFO("Hello");

          if(lowerx<tempx1 && tempx1<upperx && lowery<tempy1 && tempy1<uppery && lowerz<tempz1 && tempz1<upperz)          {
          n++;
          i3D=CCTK_GFINDEX3D(cctkGH, i, j, k);
          /*
          rho[i3D]=*(CCTK_REAL *)(output_rho+sizeof(CCTK_REAL)*n);
          eps[i3D]=*(CCTK_REAL *)(output_eps+sizeof(CCTK_REAL)*n);
          press[i3D]=*(CCTK_REAL *)(output_press+sizeof(CCTK_REAL)*n);
          w_lorentz[i3D]=*(CCTK_REAL *)(output_w_lorentz+sizeof(CCTK_REAL)*n);*/
          rho[i3D]=output_rho[n];
          eps[i3D]=output_eps[n];
          press[i3D]=output_press[n];                    
          w_lorentz[i3D]=output_w_lorentz[n];
          }
          else{
            rho[i3D]=rho[far];
            press[i3D]=press[far];
            eps[i3D]=eps[far];
            w_lorentz[i3D]=w_lorentz[far];
          }
        } 
      }
    }     
  CCTK_INFO("Rotation Done");

}






